import React, { Component } from 'react';
import { Navbar, NavbarBrand, Nav, NavbarToggler, Collapse, NavItem, } from 'reactstrap';

class Footer extends Component {

    constructor(props) {
        super(props);

    }


    render() {

        return (
            
                <Navbar light bg-white>
                    <div className="container">
                            <Nav className = "navbar fixed-bottom bg-primary align-self-center" >
                                <a className="navbar-brand mx-auto">Copyright 2019 - Subscribe</a>
                            </Nav>
                    </div>
                </Navbar>
        );

    }

}

export default Footer;