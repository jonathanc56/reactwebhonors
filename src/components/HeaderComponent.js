import React, { Component } from 'react';
import { Navbar, NavbarBrand, Nav, NavbarToggler, Collapse, NavItem } from 'reactstrap';
import { NavLink } from 'react-router-dom';

class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isNavOpen: false,
        }
        this.toggleNav = this.toggleNav.bind(this);

    }

    toggleNav() {
        this.setState({
            isNavOpen: !this.state.isNavOpen
        });
    }


    render() {

        return (
          
                <Navbar light bg-white expand="md">
                    <div className="container">
                        <NavbarToggler onClick={this.toggleNav} />
                        <NavbarBrand className="ml-auto" href="/">
                            <img src="assets/logo.jpg" 
                                alt="Subscribe" />
                        </NavbarBrand>
                        <Collapse isOpen={this.state.isNavOpen} navbar>
                            <Nav navbar>
                                <NavItem >
                                    <NavLink className="nav-link text-dark align-self-center text-center active" to="/home">
                                        <span className="fa fa-home fa-lg fa-fw" style={{ color: 'navy' }} ></span> Home             
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <div className="nav-link text-dark align-self-center text-center" to="/aboutus">
                                        <span className="fa fa-money fa-fw" style={{ color: 'navy' }} ></span> Subscriptions
                                    </div>
                                </NavItem>
                                <NavItem>
                                    <div className="nav-link text-dark align-self-center text-center" to="/menu">
                                        <span className="fa fa-list fa-fw" style={{ color: 'navy' }} ></span> About
                                    </div>
                                </NavItem>
                                <NavItem>
                                    <div className="nav-link text-dark align-self-center text-center" to="/contactus">
                                        <span className="fa fa-address-card fa-fw" style={{ color: 'navy' }} ></span> Contact
                                    </div>
                                </NavItem>
                            </Nav>

                            <Nav className = "ml-auto" >
                                <NavItem >
                                    <NavLink className="nav-link text-dark align-self-center text-center" id="leftlistitem1" to="/account">
                                        <span className="fa fa-user fa-lg fa-fw" style={{ color: 'navy' }} ></span> Account
                                    </NavLink>
                                </NavItem>
                                <NavItem >
                                    <div className="nav-link text-dark align-self-center text-center" to="/contactus">
                                        <span className="disabled text-center "></span> Sign In
                                    </div>
                                </NavItem>

                            </Nav>
                        </Collapse>
                    </div>
                </Navbar>
                
       

        );

    }

}

export default Header;