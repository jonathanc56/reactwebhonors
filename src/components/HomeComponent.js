import React from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle,  CardGroup } from 'reactstrap';



const Home = () => {

    return (

        <div>

            <section className="text-center" id="itemcontainerblue">
                <div className="container">
                    <div className="row row-content justify-content-center">

                        <div className="col-6 col-sm-4 align-self-left">
                            <img src="assets/razor.jpg" className="img-fluid" id="circleimage" />
                        </div>

                        <div className="col-12 col-sm-6 align-self-center" id="containertextright">
                            <h1 className="text-center">Pick of the Day</h1>
                            <p>Andi blant ideritibea peles vendus
                                nimpossi arum in re, ut volor re et,
                                te a quam labo. Or adiam doluptat
                                volo quas eaquo volupta aut doluptaquam
                                inveliquossi abor alitas sunt
                                veniend andus.
                        Lam abori vel eicil imagniet exp</p>
                        </div>
                    </div>
                </div>
            </section>

            <CardGroup className = "mt-4">
            <div className="col-12 col-md-4">
                <Card className = "border-info">
                    <CardBody>
                        <CardTitle> T Shirts </CardTitle>
                        <CardImg src="assets/tshirt.png" alt="T Shirt" height="250" width="100"/>
                        <CardText>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
                                            dis parturient montes, nascetur ridiculus mus.
                                            Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat
                                    massa quis enim. </p>
                            <p><small className="text-muted">Last updated 3 mins ago</small></p>
                        </CardText>
                    </CardBody>
                </Card>
                </div>

                <div className="col-12 col-md-4">
                <Card className = "border-info">
                    <CardBody>
                        <CardTitle> Sneakers </CardTitle>
                        <CardImg src="assets/sneakers.png" alt="Sneakers" height="250" width="100"/>
                        <CardText>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
                                            dis parturient montes, nascetur ridiculus mus.
                                            Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat
                                    massa quis enim. </p>
                            <p><small className="text-muted">Last updated0 3 mins ago</small></p>
                        </CardText>
                    </CardBody>
                </Card>
                </div>

                <div className="col-12 col-md-4">
                <Card className = "border-info">
                    <CardBody>
                        <CardTitle> Magazines </CardTitle>
                        <CardImg src="assets/magazine.png" alt="Magazine" height="250" width="100" />
                        <CardText>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
                                            dis parturient montes, nascetur ridiculus mus.
                                            Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat
                                    massa quis enim. </p>
                            <p><small className="text-muted">Last updated 15 mins ago</small></p>
                        </CardText>
                    </CardBody>
                </Card>
                </div>
                
            </CardGroup>
        </div>
    );

};





export default Home;