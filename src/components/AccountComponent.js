import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'reactstrap';
import { Card, CardImg, CardText, CardBody, CardTitle, FormGroup, Label, Form, Input, Button } from 'reactstrap';


class Account extends Component {

    render() {
        return (
            <React.Fragment>
                <div>
                    <Navbar light bg-white bg-white id="secondarynavbar">
                        <div className="container">

                            <Nav navbar className="mx-auto">
                                <div className="row">
                                    <NavItem >
                                        <div className="nav-link text-dark align-self-center text-center active ml-2" to="/home" id="leftlistitem2">
                                            <span></span> Your Account
                        </div>
                                    </NavItem>
                                    <NavItem>
                                        <div className="nav-link text-dark align-self-center text-center ml-2" to="/aboutus">
                                            <span></span> Manage Subscriptions
                        </div>
                                    </NavItem>
                                </div>
                            </Nav>
                        </div>
                    </Navbar>
                </div>
                <div>
                    <Card className="col-8 " id="centercard">
                        <CardTitle className="bg-primary row" id="cardtitle"> General Information </CardTitle>
                        <CardBody>
                            <Form >
                                <FormGroup row>
                                    <Label htmlFor="firstname" className="col-md col-sm-12 ml-5">First Name</Label>
                                    <Input type="text" id="firstname" name="firstname" className="col-md-8" />
                                </FormGroup>
                                <FormGroup row >
                                    <Label htmlFor="lastname" className="col-md col-sm-12 ml-5">Last Name</Label>
                                    <Input type="text" id="lastname" name="lastname" className="col-md-8" />
                                </FormGroup>
                                <FormGroup row >
                                    <Label htmlFor="dateofbirth" className="col-md col-sm-12 ml-5">Date of Birth</Label>
                                    <Input type="date" id="dateofbirth" name="dateofbirth" className="col-md-8" />
                                </FormGroup>
                            </Form>
                        </CardBody>
                    </Card>
                </div>


                <div>
                    <Card className="col-sm-12 col-md-4" id="centercard">
                        <CardTitle className="bg-primary row" id="cardtitle"> Scribe Points </CardTitle>
                        <CardBody>
                            <Form >
                                <FormGroup row >
                                    <Label htmlFor="dateofbirth" className="col-md col-sm-12 text-center">Points Available</Label>
                                </FormGroup>
                                <FormGroup row >
                                    <Label htmlFor="dateofbirth" className="col-md col-sm-12 text-center" >245</Label>
                                </FormGroup>
                            </Form>
                            <p class="card-text text-center mb-1" id="smallfont">Your points will never expire and can be redeemed be redeemed on any purchase on this site.</p>
                        </CardBody>
                    </Card>
                </div>

                <section>
                    <div className="container">
                        <div className="row">
                            <h5>Payment Method</h5>
                        </div>
                    </div>

                    <div className="container">
                        <div id="lowheading"  style={{ width: '30%' }}>
                            <img src="assets/payment.jpg" class="img-fluid" alt="Payment Methods" />
                        </div>
                    </div>
                </section>

            </React.Fragment>
        );
    }

}

export default Account;